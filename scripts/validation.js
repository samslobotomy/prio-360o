// * variables
const name = $("#name");
const email = $("#email");
const signinWith = $("#signin-with");
const signinGoogle = $("#signin-google");
const signinFacebook = $("#signin-facebook");
const form = $("#form");
// const form = document.getElementById('form'); --> vanilla JS
const registerMessage = $("#congratulation-message");
const nameValue = $("#name-value");
const closeButton = $('.close-button');

// * event handler on key up of input
// * sempre que uma tecla é solta (levantas o dedo)
// * a funçao vai ler o valor dos inputs (name e email)
// * se ambos têm alguma coisa escrita o botão muda para 'confirmar'
// * caso contrário, o botão mantém-se ou altera-se para'registar com'
$("#name").keyup(function () {
	if (name.val().trim() !== "") {
		if (email.val().trim() !== "") {
			changeButton(signinWith, "Confirmar", "");
			signinGoogle.css("opacity", "0");
			signinFacebook.css("opacity", "0");
		} else {
			changeButton(signinWith, "Registar-me com", "signin-with");
			signinGoogle.css("opacity", "1");
			signinFacebook.css("opacity", "1");
		}
	} else {
		changeButton(signinWith, "Registar-me com", "signin-with");
		signinGoogle.css("opacity", "1");
		signinFacebook.css("opacity", "1");
	}
});
$("#email").keyup(function () {
	if (email.val().trim() !== "") {
		if (name.val().trim() !== "") {
			changeButton(signinWith, "Confirmar", "");
			signinGoogle.css("opacity", "0");
			signinFacebook.css("opacity", "0");
		} else {
			changeButton(signinWith, "Registar-me com", "signin-with");
			signinGoogle.css("opacity", "1");
			signinFacebook.css("opacity", "1");
		}
	} else {
		changeButton(signinWith, "Registar-me com", "signin-with");
		signinGoogle.css("opacity", "1");
		signinFacebook.css("opacity", "1");
	}
});
// * função que recebe um elemento, um valor e um id
// * atribui a esse elemento o id passado e o valor passado como argumentos
function changeButton(element, value, id) {
	element.attr("id", id);
	element.val(value);
}
// * event click handler
// * ao clicar no botão com o texto 'Confirmar'
// * esconde o formulário
// * dá display grid à mensagem de parabéns
signinWith.click(function () {
	if (signinWith.val() === "Confirmar") {
		var nameInput = name.val();
		const nameLowerCase = nameInput.toLowerCase();
		const nameCapitalized =
			nameLowerCase.charAt(0).toUpperCase() + nameLowerCase.slice(1);
		nameValue.text(nameCapitalized);
		form.css("display", "none");
		registerMessage.css("display", "grid");
	}
});

// * fechar a mensagem de parabéns e voltar ao form
closeButton.click(function(){
	registerMessage.css("display", "none");
	form.css("display", "block");
})